from django.shortcuts import render

from contact.forms import ContactForm
from contact.models import ContactedBy


# Create your views here.

def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            contact = ContactedBy()
            contact.full_name = form.cleaned_data['full_name']
            contact.contact_mode = form.cleaned_data['contact_mode']
            contact.contact_info = form.cleaned_data['contact_info']
            contact.message = form.cleaned_data['message']
            contact.save()
            return render(request, 'contact/thanks.html', {'active': 'contact'})
    else:
        form = ContactForm()
    return render(request, 'contact/contact.html', {'form': form, 'active': 'contact'})