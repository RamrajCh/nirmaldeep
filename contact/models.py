from django.db import models

# Create your models here.

class ContactedBy(models.Model):
    CONTACT_MODE_CHOICES = (
        ('telegram', 'Telegram'),
        ('facebook_messenger', 'Facebook Messenger'),
        ('whatsapp', 'Whatsapp'),
        ('viber', 'Viber'),
        ('gmail', 'Gmail'),
        ('phone', 'Phone Number')
    )

    full_name = models.CharField(max_length=50)
    contact_mode = models.CharField(max_length=50, choices=CONTACT_MODE_CHOICES, default='whatsapp')
    contact_info = models.CharField(max_length=250)
    message = models.TextField(max_length=1000)
    message_read = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ("-created",)
    
    def __str__(self):
        return f"{self.full_name} -> {self.contact_mode} -> {self.contact_info}"