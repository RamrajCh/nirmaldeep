from django import forms

from contact.models import ContactedBy

class ContactForm(forms.ModelForm):
    class Meta:
        model = ContactedBy
        fields = ('full_name', 'contact_mode', 'contact_info', 'message')
        widgets = {
            'full_name': forms.TextInput(attrs={'class': 'input is-medium', 'placeholder': 'Full Name'}),
            'contact_info': forms.TextInput(attrs={'class': 'input is-medium', 'placeholder': 'Contact Info'}),
            'message': forms.Textarea(attrs={'class': 'textarea has-fixed-size is-medium', 'placeholder': 'Type your message here...'}),
        }