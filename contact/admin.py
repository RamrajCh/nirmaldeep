from django.contrib import admin
from contact.models import ContactedBy

# Register your models here.

@admin.register(ContactedBy)
class ContactedByAdmin(admin.ModelAdmin):
    list_display = ("full_name", "contact_mode", "contact_info", "message_read", "created")
    list_filter = ("contact_mode", "message_read", "created")
    ordering = ("message_read", "created")