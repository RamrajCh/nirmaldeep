from django.shortcuts import render, get_object_or_404

from services.models import ServiceCategory, Services

# Create your views here.


def services_list(request):
    categories = ServiceCategory.objects.all()
    if categories:
        services = {category: Services.objects.filter(category=category)  for category in categories}
    else:
        services = {}
    
    return render(
        request,
        template_name="services/list.html",
        context={
            "services": services,
            "active": "services",
        }
    )


def services_detail(request, id):
    service = get_object_or_404(klass=Services, pk=id)
    return render(
        request,
        template_name="services/detail.html",
        context={
            "service": service,
            "active": "services",
        }
    )