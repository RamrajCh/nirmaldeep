from django.db import models

# Create your models here.

class ServiceCategory(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Services(models.Model):
    name = models.CharField(max_length=250)
    category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE, related_name="services")
    description = models.TextField(max_length=5000, blank=True)
    picture_url = models.URLField(max_length=500, blank=True)
    youtube_video_url = models.URLField(max_length=500, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-created', 'category',)

    def __str__(self):
        return self.name