from django.contrib import admin
from services.models import ServiceCategory, Services

# Register your models here.

@admin.register(ServiceCategory)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']

@admin.register(Services)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'category', 'created', 'updated']
    list_filter = ['category', 'created', 'updated']