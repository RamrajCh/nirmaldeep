from django.contrib import admin
from base.models import Quote

# Register your models here.

@admin.register(Quote)
class ContactedByAdmin(admin.ModelAdmin):
    list_display = ("name", "title", "created")
    list_filter = ("created",)
    ordering = ("created",)
    search_fields = ("name",)