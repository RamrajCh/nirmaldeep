from django.urls import path
from base.views import home_page

app_name = 'base'

urlpatterns = [
    path('', home_page, name='home_page'),
]