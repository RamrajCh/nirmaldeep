from django.db import models

# Create your models here.

class EnglishQuoteManager(models.Manager):
    def get_queryset(self):
        return super(EnglishQuoteManager, self).get_queryset()\
            .filter(language="english")

class NepaliQuoteManager(models.Manager):
    def get_queryset(self):
        return super(NepaliQuoteManager, self).get_queryset()\
            .filter(language="nepali")

class Quote(models.Model):
    LANGUAGE = (
        ("english", "English"),
        ("nepali", "Nepali")
    )

    quote = models.TextField(max_length=1500)
    name = models.CharField(max_length=100)
    title = models.CharField(max_length=200)
    language = models.CharField(choices=LANGUAGE, default="english", max_length=20)
    created = models.DateTimeField(auto_now_add=True)

    objects = models.Manager()
    english_quotes = EnglishQuoteManager()
    nepali_quotes = NepaliQuoteManager()
    class Meta:
        ordering=("-created",)
    
    def __str__(self):
        return f"Quote by {self.name}"